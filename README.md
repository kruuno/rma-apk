# Fakultet ToDo list aplikacija

## AUTOR: Krunoslav Kovač

### Kratki opis aplikacije

Jednostavna Android aplikacija koja omogućuje unos kolokvija te izostanaka s fakulteta te nam ispisuje iste.

### SCREENSHOOTs

<div align="center">
    Prilikom pokretanja aplikacije korisniku su prikazani fragmenti (Kolokviji i Izostanci) <br/>
  <img src="images/1.png" width="200" height="400" /> <br/>
</div>

<div align="center">
    Korisnik odabire fragment. <br/>
  <img src="images/2.png" width="200" height="400" /> 
  <img src="images/3.png" width="200" height="400" /> <br/>
</div>

<div align="center">
    Dodavanje novog zadatka <br/>
  <img src="images/4.png" width="200" height="400" /> 
  <img src="images/5.png" width="200" height="400" /> <br/>
</div>

<div align="center">
    Klikom na gumb X korisnik briše određeni zadatak <br/>
  <img src="images/6.png" width="200" height="400" /> <br/>
</div>