package krunoslavkovac.ferit.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView tv_task, displayDate;
    Button btn_delete;
    RelativeLayout parentLayout;

    public ViewHolder(@NonNull View itemView, OnClickInterface onClickInterface) {
        super(itemView);
        tv_task = (TextView) itemView.findViewById(R.id.tv_task);
        displayDate = (TextView) itemView.findViewById(R.id.tv_display_date);
        btn_delete = (Button) itemView.findViewById(R.id.btn_delete);
        parentLayout = (RelativeLayout) itemView.findViewById(R.id.parentLayout);
        itemView.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}