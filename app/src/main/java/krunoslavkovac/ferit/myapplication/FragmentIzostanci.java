package krunoslavkovac.ferit.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import krunoslavkovac.ferit.myapplication.model.Izostanci;
import krunoslavkovac.ferit.myapplication.model.RealmHelper;

public class FragmentIzostanci extends Fragment implements OnClickInterface{

    private ArrayList<String> mTasks = new ArrayList<>();
    Button btn_add_izostanak;
    RecyclerView recyclerView;
    RecyclerViewAdapterIzostanci adapter;
    Realm realm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.izostanci_fragment, container, false);

        btn_add_izostanak = (Button) view.findViewById(R.id.btn_open_izostanci);

        //SETUP RV
        recyclerView = view.findViewById(R.id.recyclerViewIzostanci);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //SETUP REALM
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Izostanci")
                .build();
        realm = Realm.getInstance(realmConfiguration);

        //get data
        RealmHelper helper = new RealmHelper(realm);
        mTasks = helper.retriveIzostanci();

        //BIND
        adapter = new RecyclerViewAdapterIzostanci( mTasks, this, getContext());
        recyclerView.setAdapter(adapter);

        openIzostanciActivity();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Izostanci")
                .build();
        realm = Realm.getInstance(realmConfiguration);
    }


    @Override
    public void onDeleteClicked(String name) {
        final Izostanci realmResult = realm.where(Izostanci.class).equalTo("name",name).findFirst();
        realm.executeTransaction( new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Izostanci.deleteFromRealm(realmResult);
            }
        });
    }

    private void openIzostanciActivity(){

        btn_add_izostanak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddIzostanciActivity.class);
                startActivity(intent);
            }
        });
    }
}
