package krunoslavkovac.ferit.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import krunoslavkovac.ferit.myapplication.model.RealmHelper;
import krunoslavkovac.ferit.myapplication.model.Tasks;

public class FragmentKolokviji extends Fragment implements OnClickInterface {

    private ArrayList<String> mTasks = new ArrayList<>();
    private ArrayList<String> mDates = new ArrayList<>();
    Button btn_open_add;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    private Realm realm;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kolokviji_fragment, container, false);

        btn_open_add = (Button) view.findViewById(R.id.btn_open_add);

        openAddActivity();

        //SETUP RV
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //SETUP REALM
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Tasks")
                .build();
        realm = Realm.getInstance(realmConfiguration);

        //get data
        RealmHelper helper = new RealmHelper(realm);
        mTasks = helper.retrive();
        mDates = helper.retriveDates();

        //BIND
        adapter = new RecyclerViewAdapter( mTasks, mDates, this,getContext());
        recyclerView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Realm.init(getContext());
        RealmConfiguration realmConfiguration1 = new RealmConfiguration.Builder()
                .name("Tasks")
                .build();
        realm = Realm.getInstance(realmConfiguration1);
    }


    @Override
    public void onDeleteClicked(String task) {
        final Tasks realmResult = realm.where(Tasks.class).equalTo("tasks",task).findFirst();
        realm.executeTransaction( new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Tasks.deleteFromRealm(realmResult);
            }
        });
    }

    private void openAddActivity(){
        btn_open_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddKolokvijActivity.class);
                startActivity(intent);
            }
        });
    }

}
