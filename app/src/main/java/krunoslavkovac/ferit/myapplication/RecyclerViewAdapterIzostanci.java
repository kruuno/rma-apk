package krunoslavkovac.ferit.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;


public class RecyclerViewAdapterIzostanci extends RecyclerView.Adapter<ViewHolderIzostanci> {


    private ArrayList<String> mTv_tasks = new ArrayList<>();
    private Context mContext;
    OnClickInterface onClickInterface;


    public RecyclerViewAdapterIzostanci(ArrayList<String> mTv_tasks, OnClickInterface onClickInterface, Context mContext) {
        this.mTv_tasks = mTv_tasks;
        this.mContext = mContext;
        this.onClickInterface = onClickInterface;
    }

    @NonNull
    @Override
    public ViewHolderIzostanci onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.izostanci_item, viewGroup, false);
        ViewHolderIzostanci viewHolder = new ViewHolderIzostanci(view, onClickInterface);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderIzostanci viewHolder, final int position) {
        viewHolder.tv_task_izostanak.setText(mTv_tasks.get(position));

        //Delete task
        viewHolder.btn_delete_izostanak.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)
            {
                onClickInterface.onDeleteClicked(mTv_tasks.get(position));
                mTv_tasks.remove(position);
                notifyItemRemoved(position);
                notifyItemChanged(position, mTv_tasks.size());
                Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, mTv_tasks.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTv_tasks.size();
    }
}