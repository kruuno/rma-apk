package krunoslavkovac.ferit.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import krunoslavkovac.ferit.myapplication.model.Izostanci;
import krunoslavkovac.ferit.myapplication.model.RealmHelper;
import krunoslavkovac.ferit.myapplication.model.Tasks;

public class AddIzostanciActivity extends AppCompatActivity {
    private EditText et_add;
    private Button btn_add;

    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_izostanak);
        et_add = (EditText) findViewById(R.id.et_add_izostanak);
        btn_add = (Button) findViewById(R.id.btn_add_izostanak);


        //SETUP REALM
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Izostanci")
                .build();
        realm = Realm.getInstance(realmConfiguration);

        addIzostanak();
    }

    private void addIzostanak() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Izostanci t = new Izostanci();
                String value = et_add.getText().toString();
                t.setName(value);

                RealmHelper helper = new RealmHelper(realm);
                if (value.matches("")) {
                    Toast.makeText(getApplicationContext(), "Cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                helper.saveIzostanci(t);
                et_add.setText("");

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
