package krunoslavkovac.ferit.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewHolderIzostanci extends RecyclerView.ViewHolder implements View.OnClickListener{

    TextView tv_task_izostanak;
    Button btn_delete_izostanak;
    RelativeLayout parentLayout;

    public ViewHolderIzostanci(@NonNull View itemView, OnClickInterface onClickInterface) {
        super(itemView);
        tv_task_izostanak = (TextView) itemView.findViewById(R.id.tv_izostanak);
        btn_delete_izostanak = (Button) itemView.findViewById(R.id.btn_delete_izostanak);
        parentLayout = (RelativeLayout) itemView.findViewById(R.id.parentLayoutIzostanak);
        itemView.setOnClickListener(this);
        btn_delete_izostanak.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}