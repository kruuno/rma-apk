package krunoslavkovac.ferit.myapplication.model;

import io.realm.RealmObject;

public class Izostanci extends RealmObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
