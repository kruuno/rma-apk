package krunoslavkovac.ferit.myapplication.model;



import io.realm.RealmObject;

public class Tasks extends RealmObject {
    private String tasks;
    private String  date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTasks() {
        return tasks;
    }

    public void setTasks(String tasks) {
        this.tasks = tasks;
    }
}
