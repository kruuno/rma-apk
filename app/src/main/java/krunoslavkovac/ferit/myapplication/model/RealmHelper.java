package krunoslavkovac.ferit.myapplication.model;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {
    Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    //WRITE TO DB

    public void save(final Tasks tasks){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Tasks t = realm.copyToRealm(tasks);
            }
        });

    }

    //READ FROM DB
    public ArrayList<String> retrive(){
        ArrayList<String> tTasks = new ArrayList<>();
        RealmResults<Tasks> tasks = realm.where(Tasks.class).findAll();

        for(Tasks t:tasks){
            tTasks.add(t.getTasks());
        }
        return tTasks;
    }

    public ArrayList<String> retriveDates(){
        ArrayList<String> mDates = new ArrayList<>();
        RealmResults<Tasks> dates = realm.where(Tasks.class).findAll();

        for(Tasks t:dates){
            mDates.add(t.getDate());
        }
        return mDates;
    }

    //WRITE TO DB IZOSTANCI

    public void saveIzostanci(final Izostanci izostanci){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Izostanci t = realm.copyToRealm(izostanci);
            }
        });
    }

    //READ FROM DB IZOSTANCI
    public ArrayList<String> retriveIzostanci(){
        ArrayList<String> tTasks = new ArrayList<>();
        RealmResults<Izostanci> izostanci = realm.where(Izostanci.class).findAll();

        for(Izostanci t:izostanci){
            tTasks.add(t.getName());
        }
        return tTasks;
    }

}
