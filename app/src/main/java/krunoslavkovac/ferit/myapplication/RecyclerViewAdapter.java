package krunoslavkovac.ferit.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<String> mTv_tasks = new ArrayList<>();
    private ArrayList<String> mTv_dates = new ArrayList<>();
    private Context mContext;
    OnClickInterface onClickInterface;


    public RecyclerViewAdapter(ArrayList<String> mTv_tasks, ArrayList<String> mTv_dates, OnClickInterface onClickInterface, Context mContext) {
        this.mTv_tasks = mTv_tasks;
        this.mTv_dates = mTv_dates;
        this.mContext = mContext;
        this.onClickInterface = onClickInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onClickInterface);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        viewHolder.tv_task.setText(mTv_tasks.get(position));
        viewHolder.displayDate.setText(mTv_dates.get(position));

        //Delete task
        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v)
            {
                onClickInterface.onDeleteClicked(mTv_tasks.get(position));
                mTv_tasks.remove(position);
                mTv_dates.remove(position);
                notifyItemRemoved(position);
                notifyItemChanged(position, mTv_tasks.size());
                Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, mTv_tasks.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTv_tasks.size();
    }
}