package krunoslavkovac.ferit.myapplication;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import krunoslavkovac.ferit.myapplication.model.RealmHelper;
import krunoslavkovac.ferit.myapplication.model.Tasks;

public class AddKolokvijActivity extends AppCompatActivity{
    private EditText et_add;
    private Button btn_add;
    private TextView tv_date;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kolokvij);
        et_add = (EditText) findViewById(R.id.et_add_kolokvij);
        btn_add = (Button) findViewById(R.id.btn_add_kolokvij);

        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        AddKolokvijActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }

        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                tv_date.setText(date);
            }
        };

        //SETUP REALM
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Tasks")
                .build();
        realm = Realm.getInstance(realmConfiguration);

        addKolokvij();
    }

    public void addKolokvij(){
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tasks t = new Tasks();
                String value = et_add.getText().toString();
                String date = tv_date.getText().toString();
                value = value.replaceAll("\n"," ");
                t.setTasks(value);
                t.setDate(date);

                RealmHelper helper = new RealmHelper(realm);
                if (value.matches("")) {
                    Toast.makeText(getApplicationContext(), "You did not enter a task", Toast.LENGTH_SHORT).show();
                    return;
                }
                helper.save(t);
                et_add.setText("");

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
